import axiosClient from './api'
import apiConfig from '../utils/api-config'



class CarProvider {

	getAllCars() {
        return axiosClient
            .get(apiConfig.carsUrl, data)
    }

    getCarDetail(id) {
        return axiosClient
            .post(apiConfig.carsUrl+'/'+id, data)
    }
}


export default new CarProvider();