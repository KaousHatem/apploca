import axiosClient from './api'
import apiConfig from '../utils/api-config'



class AuthProvider {

	login( data ) {
        return axiosClient
            .post(apiConfig.loginUrl, data)
    }

    signUp( data ) {
        return axiosClient
            .post(apiConfig.SignUpUrl, data)
    }
}


export default new AuthProvider();