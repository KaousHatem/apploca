import React, { Fragment, useEffect, useState } from "react";
import Header from "../../components/header";
import Hero from "../../components/hero";
import FindCar from "../../components/findcar";
import About from "../../components/About";
import Service from "../../components/Service";
import Promo from "../../components/Promo";
import HotOffers from "../../components/HotOffers";
import Testimonial from "../../components/Testimonial";
import Team from "../../components/Team";
import Help from "../../components/Help";
import Blog from "../../components/Blog";
import Footer from "../../components/Footer";
import { cars } from "../../__mock__/cars";

const HomePage = () => {

  const [allCars, setAllCars] = useState([])
  const [count, setCount] = useState(0)
  // const allCars = cars

  const loadData = () => {
    // load cars from api
    
    console.log(cars)

  }

  useEffect(()=>{

    setAllCars(cars)
  },[allCars])

  return ( allCars.length &&
    <Fragment>
      <Header />
      <Hero />
      <FindCar />
      <About />
      <Service />
      <Promo />
      <HotOffers allCars={allCars} />
      <Testimonial />
      <Team />
      <Help />
      <Blog />
      <Footer />
    </Fragment>
  );
};
export default HomePage;
