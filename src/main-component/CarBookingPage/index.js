import React, { Fragment, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";

import Header from "../../components/header";
import PageTitle from "../../components/PageTitle";
import CarBooking from "../../components/CarBooking";
import Footer from "../../components/Footer";
import { cars } from "../../__mock__/cars";

const CarBookingPage = () => {
  const { t } = useTranslation();

  const [carDetail, setCarDetail] = useState()


  useEffect(()=>{
    if(!carDetail){
      
      setCarDetail(cars[0])
    }
    
  },[carDetail])

  return ( carDetail && 
    <Fragment>
      <Header />
      <PageTitle
        pageTitle={t("header-navigation.car_booking")}
        pagesub={t("header-navigation.car_booking")}
      />
      <CarBooking carDetail={carDetail} />
      <Footer />
    </Fragment>
  );
};
export default CarBookingPage;
